# Phd-Thesis template

You can start editing `Thesis.lyx`. As an example of how the template looks, I include my thesis in `Thesis_Final.pdf`.

[TOC]

## Editing

Always open the main file `Thesis.lyx` and edit the different chapters from there by double clicking on the chapter reference.
That way, the bibliography and custom lyx commands are available when you edit chapters. If you open a chapter separately, they won't be available.

Also always compile the main file. If you just want to compile a single chapter, put the reference to the other chapters in comment.

## Custom Lyx commands

To keep all symbols throughout the thesis consistent, it is best that you define your set of custom lyx commands and use those all the time in all your formulas. This will allow you to change the name of a variable in a single place and it will automatically be updated throughout the whole thesis.

To add a custom lyx command, press the 'insert math macro' button on the toolbar:

![](/Extras/mathmacrobutton.png)

When you have added your command (`\adam`, for example), you can use it in math mode by just typing `\adam`. When lyx recognises the command, it will replace the `\adam` by whatever you have defined it to be.

My lyx commands are placed in the `config\CustomCommands.lyx` file, which is included at the very beginning of `Thesis.lyx`. It inclues a whole bunch of different commands.

Usefull ones include

+ `\wk` to add `\!\left( j \omega_k \right)`
+ `\t` to add `\!\left( t \right)`
+ `\BLA` to add the superscript `^\mathrm{BLA}` to a variable

The 'List of Symbols' at the end of the thesis is also very easy to generate when you have all your commands defined nicely: you just make a table with each of the commands in it and you're done.

## Glossaries

A very usefull package I use to deal with abbreviations is the `Glossaries` package. It allows to define a list of acronyms and allows to reference those acronyms with the `\gls` command.
The package will automatically replace the first reference by the full term and all following references will be abbreviated.

For example: when I have defined BLA as Best Linear Approximation

```latex
The \gls{BLA} is very usefull, with the \gls{BLA}, we can do all kinds of stuff.
```
will compile into
```
The Best Linear Approximation (BLA) is very usefull, with the BLA, we can do all kinds of stuff.
```
Some other usefull commands from the Glossaries package include

+ `\glspl` to reference a plural (`\glspl{BLA}` becomes BLAs)
+ `\glsentryfirst` to force a term to be put in full.
+ `\glsresetall` to reset all abbreviations. I do this in the beginning of every chapter

The list of abbreviations can be found in the `config\acronyms.tex` file, which is included in `preamble.tex`.

At the very last compilation of your thesis, you will need to compile with `glossaries.exe`  besides `pdflatex`  to generate the list of abbreviations:

https://tex.stackexchange.com/questions/6845/compile-latex-with-bibtex-and-glossaries

## Figures with matlab2tikz

I made most of my figures using tikz and matlab2tikz. I didn't try to get the figures perfect using only matlab and tikz. I cleaned up most of my figures manually in inkscape.

Just including tikz figures drastically increases the compile time and you will run into memory problems after a while. On my ELEC computer, I managed to increase the tikz memory size (and ended up breaking my miktex intall doing this, but Sven helped me out). In the end, even the increased memory size was not enough, so I switched to external compilation.

When you use the `standalone` option in matlab2tikz, the fonts will not be correct and the size of the figures will be difficult to control.

I used external compilation, which gives very good results, but this does not work well with lyx (I see that they improved this in lyx 2.3, but haven't tried it out). To do external compilation, I needed to compile the latex source with `pdflatex --enable-write18`  every time, which is a hassle.

I made a small lyx file `Figurecompilation.lyx`  which loads all the preambles and custom stuff of the thesis template, so that you can input your tikz figures in there and compile them standalone. You can then open the generated pdf with inkscape (use poppler to keep the fonts right) and clean up the figure. 

## Reference style

I use the reference style defined in `wmaainf.bst` for the citations. They contain the first four letters of the first author and the year.
This is the reference style closest to the one used in the book of Piet Wambacq I could find.

## List of Publications

I did not try to do fancy stuff with my Bibtex to put my own publications in a separate section in the bibliography. Instead, I just made a table which contains all my publications together with a `\cite`.
As a result, all my own publications will appear twice: once in that table and a second time in the bibliography.

## Annoying Yves

I really liked the way I included examples in my thesis, by placing them in gray boxes. Yves really didn't like that, but if you want to add my nice boxes as well, you should do the following:

1. Define the box color in the preamble: `\definecolor{exampleboxcolor}{cmyk}{0,0,0,0.2}`
2. Use the tcolorbox package `\usepackage[most]{tcolorbox}`
3. Make sure the boxes look nice: `\tcbset{colback=white,boxrule=2pt,colframe=exampleboxcolor}`
4. Create the example command: `\newcommand{\example}[1]{\begin{tcolorbox}[breakable] #1 \end{tcolorbox}}`

Now, anywhere in the thesis, you can add a very nice looking box using `\example{boxcontent}`

## Weird font error on windows 10

On my laptop I got the following error while compiling:

```
pdfTeX error (font expansion): auto expansion is only possible with scalable fonts.
```

This post has the solution for this:

https://tex.stackexchange.com/questions/10706/pdftex-error-font-expansion-auto-expansion-is-only-possible-with-scalable

The following steps fixed the problem on my laptop:

1. Start "MiKTeX Package Manager (Admin)"
2. Install the `cm-super` package.
3. On the command line, run `initexmf --mkmaps`

