#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
\input{../config/preamble.tex}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize b5paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 1
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
List of symbols
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="21" columns="2">
<features islongtable="true" longtabularalignment="center">
<column alignment="right" valignment="top" width="20col%">
<column alignment="left" valignment="top" width="70col%">
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbb{N}$
\end_inset

, 
\begin_inset Formula $\mathbb{Z}$
\end_inset

, 
\begin_inset Formula $\mathbb{R}$
\end_inset

 and 
\begin_inset Formula $\mathbb{C}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Natural, Integer, Real and Complex numbers
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbb{C}^{N\times M}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Set of 
\begin_inset Formula $N\times M$
\end_inset

 matrices filled with complex numbers
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $j$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Imaginary unit 
\begin_inset Formula $j=\sqrt{-1}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\norm{\cdot}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Norm of a complex number
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\Re\left\{ \cdot\right\} $
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Real part of a complex number
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\Im\left\{ \cdot\right\} $
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Imaginary part of a complex number
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\overline{X}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Complex conjugate
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $x$
\end_inset

 or 
\begin_inset Formula $X$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Scalar
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{x}$
\end_inset

 or 
\begin_inset Formula $\mathbf{X}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Vector or matrix
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\left[\mathbf{X}\right]_{i,j}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Element in the matrix 
\begin_inset Formula $\mathbf{X}$
\end_inset

 on the 
\begin_inset Formula $i^{\mathrm{th}}$
\end_inset

 column and 
\begin_inset Formula $j^{\mathrm{th}}$
\end_inset

 row.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{I}_{N}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Identity matrix of size 
\begin_inset Formula $N$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{0}_{N\times M}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Matrix of size 
\begin_inset Formula $N\times M$
\end_inset

 filled with zeroes
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{X}^{\mathrm{T}}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Matrix transpose
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multicolumn="1" alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\vecop\left(\mathbf{X}\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The vec operator stacks the columns of a matrix 
\begin_inset Formula $\mathbf{X}$
\end_inset

 on top of each-other.
 In 
\begin_inset Formula $\mathbf{X}$
\end_inset

 is an 
\begin_inset Formula $M\times N$
\end_inset

 matrix, 
\begin_inset Formula $\mathrm{vec}\left(\mathbf{X}\right)$
\end_inset

 is a column vector of length 
\begin_inset Formula $MN$
\end_inset

.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\kron$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Kronecker product
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{X}^{\herm}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Hermitian.
 The hermitian is the complex conjugate of the transpose of a matrix
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbb{E}\left\{ \cdot\right\} $
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Expected value of a number
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $x\t$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A signal in the time domain
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $X\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A signal in the frequency domain at frequency bin 
\begin_inset Formula $k$
\end_inset

.
 The signal is measured in steady-state.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{X}\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A vector of signals all evaluated at frequency bin 
\begin_inset Formula $k$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Index used to indicate a frequency corresponding to frequency bin 
\begin_inset Formula $k$
\end_inset

 on a function that is continuous
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="29" columns="2">
<features islongtable="true" longtabularalignment="center">
<column alignment="right" valignment="top" width="20col%">
<column alignment="left" valignment="top" width="70col%">
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Frequency Response Functions
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $G_{X\rightarrow Y}\LIN\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Small-signal frequency response from signal 
\begin_inset Formula $X$
\end_inset

 to signal 
\begin_inset Formula $Y$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $G_{X\rightarrow Y}\BLA\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{SISO}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{BLA}
\end_layout

\end_inset

 from signal 
\begin_inset Formula $X$
\end_inset

 to signal 
\begin_inset Formula $Y$
\end_inset

.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{G}_{X\rightarrow\mathbf{Y}}\BLA\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{SIMO}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{BLA}
\end_layout

\end_inset

 from signal 
\begin_inset Formula $X$
\end_inset

 to vector of signals 
\begin_inset Formula $\mathbf{Y}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{G}_{\mathbf{X}\rightarrow\mathbf{Y}}\BLA\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{MIMO}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{BLA}
\end_layout

\end_inset

 from vector of signals 
\begin_inset Formula $\mathbf{X}$
\end_inset

 to vector of signals 
\begin_inset Formula $\mathbf{Y}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{C}_{\mathrm{vec}\left(\mathbf{G}_{\mathbf{X}\rightarrow\mathbf{Y}}\BLA\right)}\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Covariance matrix which expresses the uncertainty on the 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{BLA}
\end_layout

\end_inset

-estimate.
 When the 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
gls{BLA}
\end_layout

\end_inset

 has 
\begin_inset Formula $n_{u}$
\end_inset

 inputs and 
\begin_inset Formula $n_{y}$
\end_inset

 outputs, the size of this covariance matrix is 
\begin_inset Formula $n_{u}n_{y}\times n_{u}n_{y}$
\end_inset

.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
System-level signals
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $r\t$
\end_inset

 or 
\begin_inset Formula $R\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Reference signal
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $u\t$
\end_inset

 or 
\begin_inset Formula $U\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Input signal of the system under test
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $y\t$
\end_inset

 or 
\begin_inset Formula $Y\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Output of the system under test
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\dd\t$
\end_inset

 or 
\begin_inset Formula $\D\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Distortion introduced by the system under test
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{M}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Feedback dynamics
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{E}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Generator dynamics
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{O}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Output dynamics
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Circuit level signals
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $V\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Voltage in a circuit, The voltage signal is measured in steady-state and
 transformed to the frequency domain
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $I\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Current measured in a circuit
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $Z\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Impedance of an element
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $A\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Incident wave
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $B\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Reflected wave
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\Gamma\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Reflection factor
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{S}_{\mathbf{A}\rightarrow\mathbf{B}}\LIN\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
S-parameters which describe the circuit with incident waves 
\begin_inset Formula $\mathbf{A}$
\end_inset

 and reflected waves 
\begin_inset Formula $\mathbf{B}$
\end_inset

.
 
\begin_inset Formula $\mathbf{S}$
\end_inset

-matrices are always square matrices.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{S}_{\mathbf{A}\rightarrow\mathbf{B}}\BLA\wk$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\SBLA$
\end_inset

 of the circuit with incident waves 
\begin_inset Formula $\mathbf{A}$
\end_inset

 and reflected waves 
\begin_inset Formula $\mathbf{B}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\Dvec\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Distortion introduced by the circuit.
 When the circuit has 
\begin_inset Formula $N$
\end_inset

 ports, 
\begin_inset Formula $\mathbf{D}\k$
\end_inset

 is a column vector of length 
\begin_inset Formula $N$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{C}_{\Dvec}\k$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Covariance matrix of the distortion vector
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{P}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Package of the circuit
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathbf{L}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Load of the circuit
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="26" columns="2">
<features islongtable="true" longtabularalignment="center">
<column alignment="right" valignment="top" width="20col%">
<column alignment="left" valignment="top" width="70col%">
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Variables related to multisines
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\fres$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Frequency resolution of the multisine
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $A_{k}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Amplitude of the 
\begin_inset Formula $k^{\mathrm{th}}$
\end_inset

 frequency bin of the multisine
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\mathrm{\phi_{k}}$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Phase of the 
\begin_inset Formula $k^{\mathrm{th}}$
\end_inset

 frequency bin of a phase realisation the multisine
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\fmin$
\end_inset

 and 
\begin_inset Formula $\kmin$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Minimum excited frequency of the multisine.
 
\begin_inset Formula $\kmin=\nicefrac{\fmin}{\fres}$
\end_inset

 indicates the corresponding frequency bin for 
\begin_inset Formula $\fmin$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\fmax$
\end_inset

 and 
\begin_inset Formula $\kmax$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Maximum excited frequency of the multisine.
 
\begin_inset Formula $\kmax=\nicefrac{\fmax}{\fres}$
\end_inset

 indicates the corresponding frequency bin for 
\begin_inset Formula $\fmax$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\fcenter$
\end_inset

 and 
\begin_inset Formula $\kcenter$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Center frequency of the excited band of the multisine.
 
\begin_inset Formula $\kcenter=\nicefrac{\fcenter}{\fres}$
\end_inset

 indicates the corresponding frequency bin for 
\begin_inset Formula $\fcenter$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\fbandwidth$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Bandwidth of the bandpass multisine: 
\begin_inset Formula $\fbandwidth=\fmax-\fmin$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Symbols related to simulations
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\NL$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Order of non-linearity of the circuit
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\tsample$
\end_inset

 
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Sample time used in time domain simulations
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\fsample$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Sample frequency used in time domain simulations
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\tstop$
\end_inset

 
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Stop time used in time domain simulations
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Symbols related to stability analysis
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $\wex$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Base pulsation of a periodic time-varying circuit.
 
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
\lang english
\begin_inset Formula $\impls$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Trans-impedance between nodes 
\begin_inset Formula $m$
\end_inset

 and 
\begin_inset Formula $n$
\end_inset

 in the circuit.
 
\begin_inset Formula $l\in\mathbb{Z}$
\end_inset

 indicates the frequency translation 
\begin_inset Formula $l\omega_{\mathrm{ex}}$
\end_inset

 in the impedance.
 The trans-impedances are written in a sine and cosine basis such that they
 are Hermitian functions.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Formula $L^{2}\left(j\mathbb{R}\right)$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Set of functions that are square integrable on the imaginary axis
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell multicolumn="1" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Colors used to plot spectra
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Black
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Excited frequency lines of a spectrum
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\color red
Red
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Odd-order non-linear distortion
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\color blue
Blue
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Even-order non-linear distortion
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\color magenta
Magenta
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A mix between odd and even-order non-linear distortion
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\color gray
Grey
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Other frequency lines which normally contain only numerical noise
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
printglossary[type=
\backslash
acronymtype,style=longheader,toctitle={List of Abbreviations}, title={List
 of Abbreviations}]
\end_layout

\end_inset


\end_layout

\end_body
\end_document
