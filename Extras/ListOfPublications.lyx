#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
\input{../config/preamble.tex}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize b5paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 1
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
List of Publications
\end_layout

\begin_layout Subsubsection
Journal publication
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top" width="15col%">
<column alignment="left" valignment="top" width="80col%">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2013"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman, G.
 Vandersteen, and Y.
 Rolain.
 “Finding the dominant source of distortion in two-stage op-amps”.
 
\emph on
Analog Integrated Circuits and Signal Processing
\emph default
, Vol.
 78, No.
 1, pp.
 153–163, January 2013.
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Subsubsection
Workshop
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top" width="15col%">
<column alignment="left" valignment="top" width="80col%">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Vandersteen2014"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
G.
 Vandersteen, and A.
 Cooman.
 “Nonlinear distortion analysis of circuits and systems”.
 Tutorial 11 at the
\emph on
 2014 International Symposium on Circuits and Systems (ISCAS 2014)
\emph default
, Melbourne, Australia, June 1, 2014
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Subsubsection
Journal publications under review
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top" width="15col%">
<column alignment="left" valignment="top" width="80col%">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Schoukens2017"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
M.
 Schoukens, J.
 Hammenecker, and A.
 Cooman.
 “Obtaining the Pre- Inverse of a Power Amplifier using Iterative Learning
 Control”.
 Submitted to the 
\emph on
IEEE transactions on Microwave Theory and Techniques
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2017"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman, F.
 Seyfert, M.
 Olivi, S.
 Chevillard, and L.
 Baratchart.
 “Model-Free Closed-Loop Stability Analysis: A Linear Functional Approach”.
 Submitted to the 
\emph on
IEEE transactions on Microwave Theory and Techniques
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2017b"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman, P.
 Bronders, D.
 Peumans, G.
 Vandersteen, and Y.
 Rolain.
 “Distortion Contribution Analysis with the Best Linear Approximation”.
 Submitted to the 
\emph on
IEEE Transactions on Circuits and Systems I: Regular Papers
\emph default
, 2017.
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Subsubsection
Conference publications
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="2">
<features islongtable="true" longtabularalignment="left">
<column alignment="left" valignment="top" width="15col%">
<column alignment="left" valignment="top" width="80col%">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2012a"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman, E.
 Geerardyn, G.
 Vandersteen, and Y.
 Rolain.
 “Determining the Dominant Nonlinear Contributions in a multistage Op-amp
 in a Feedback Configuration”.
 In: 
\emph on
Proceedings of the 2012 International Conference on Synthesis, Modeling,
 Analysis and Simulation Methods and Applications to Circuit Design (SMACD)
\emph default
, september 2012.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2014"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman and G.
 Vandersteen.
 “Distortion Contribution Analysis by combining the Best Linear Approximation
 and noise analysis”.
 In: 
\emph on
Proceedings of the 2014 International Symposium on Circuits and Systems
 (ISCAS 2014)
\emph default
, pp.
 2772 – 2775, 2014.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2015"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman and G.
 Vandersteen.
 “Wideband Distortion Contribution Analysis of Analog Circuits with Differential
 Signalling”.
 In: 
\emph on
Proceedings of the 2015 International Conference on Synthesis, Modeling,
 Analysis and Simulation Methods and Applications to Circuit Design (SMACD)
\emph default
, 2015.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2016"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman, P.
 Bronders, and G.
 Vandersteen.
 “Distortion Contribution Analysis of strongly non-linear analog circuits”.
 In: 
\emph on
Proceedings of the 2016 International Conference on Synthesis, Modeling,
 Analysis and Simulation Methods and Applications to Circuit Design (SMACD)
\emph default
, 2016.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Peumans2016"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
D.
 Peumans, A.
 Cooman, and G.
 Vandersteen.
 “Analysis of Phase-Locked Loops using the Best Linear Approximation”.
 In: 
\emph on
Proceedings of the 2016 International Conference on Synthesis, Modeling,
 Analysis and Simulation Methods and Applications to Circuit Design (SMACD)
\emph default
, 2016.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset CommandInset citation
LatexCommand cite
key "Cooman2016b"

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
A.
 Cooman, F.
 Ferranti, E.
 Louarroudi, Y.
 Rolain, and G.
 Vandersteen.
 “Common-denominator modelling for stability analysis of electronic circuits”.
 In: 
\emph on
Proceedings of the 11th European Microwave Integrated Circuits Conference,
 
\emph default
2016.
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout Standard

\end_layout

\end_body
\end_document
